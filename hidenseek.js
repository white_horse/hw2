const random = require('./random.js');
const fs = require('fs');

function hide  (path, pokemonList, whenDone){
  //console.log('hide runs');
  //for ( let p of pokemonList){
  //  p.show();
  //}
  // var n = pokemonList.length;

//  for (let i=0;i<1000; i++){
    var nToHide = random(1, 3); // random number of pokemons to hide

    if (nToHide > pokemonList.length){
      nToHide = pokemonList.length;
    }
  //}
//console.log(nToHide);

var ixx = new Array();

  // select random nToHide
  for (let i = 0; i< nToHide; i++){
    let a = random(0, pokemonList.length - i -1);

    while (ixx.includes(a)){
      a++;
      if (a==pokemonList.length){
        a = 0;
      }
    }
    ixx.push(a);
  }

  //console.log(ixx);
  // randomly select n pokemons

 mkHiddenDir(
    ()=>KeepHiding(ixx, pokemonList, ()=>finishedHiding(ixx, pokemonList, whenDone))
  );


};

function finishedHiding(ixx, pokemonList, whenDone){
  //console.log('finished hiding runs');
  //console.log(ixx);
  //console.log(pokemonList);
  var res = new Array();
  for (let i of ixx){
    res.push(pokemonList[i]);
  }
  whenDone(res);
}


function KeepHiding(ixx, pokemonList, whenDone){
  // ixx contains pokemonNumbers to hide
  //console.log('Keep Hiding ! ')
  //console.log(ixx);
  //console.log(pokemonList);


  for(let i of ixx){
    let p = pokemonList[i];
    let folderName = `0${i}`;
    //console.log(folderName);

    var counter = 0;
    // write pokemon p into folderName:
    fs.writeFile(`field\\${folderName}\\pokemon.txt`, `${p.name}|${p.level}`, err=>{
      if (err) throw err;

      // when done - then return whenDone
      //console.log(counter);
      counter++;
      if (counter==ixx.length){

      return whenDone(ixx, pokemonList);
      }
    });

  }
}

function mkDir_complete(arr, i, whenDone){
  //console.log(`Created folder ${i}`);
  arr.push(i);
  if (arr.length==10){
    // finished creating 10 folders
    //console.log('done');
    return whenDone();
  }
}

function mkHiddenDir(whenDone){
  // creates 10 folders in the root path
  arr = new Array();
  for (let i=0;i<10;i++){
    fs.mkdir('field\\0'+i.toString(), err=>{
      if (err) throw err;
      return mkDir_complete(arr, i, whenDone);
    });
  }
}

function seek (path) {
  console.log('seek runs');
};


module.exports = {hide, seek};
