class Pokemon{
  show(){
    console.log(`${this.name}-${this.level}`);
  }
  constructor(name, level){
    this.name = name;
    this.level = level;
  }
}

module.exports = Pokemon;
