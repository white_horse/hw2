// main module
const PokemonList = require('./PokemonList.js');
const Pokemon = require('./Pokemon.js');
const utils = require('./hidenseek.js');

var pList =
[ {name:'f1', level:10}
,{name:'f2', level:2},
/*{name:'a3', level:222},
{name:'f4', level:22},
{name:'f5', level:210},
 {name:'f6', level:32},
 {name:'a7', level:4222},
 {name:'f8', level:522},*/
]
.map(p=>new Pokemon(p.name, p.level));

utils.hide('', pList, (r)=>console.log(r));
