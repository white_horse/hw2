const Pokemon = require('./Pokemon.js');

class PokemonList extends Array{
  constructor (...items){
    super();
    if (items){
      // console.log('adding');

      for (let p of items){
        // console.log(p);
        if (p instanceof Pokemon){
          super.push(p);
        }
      }
    }
  }

  add(name, level){
    var p= new Pokemon(name, level);
    super.push(p);
  }
}

module.exports = PokemonList;
